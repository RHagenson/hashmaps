package standard

import (
	"sync"

	"bitbucket.org/rhagenson/hashmaps"
)

// Map is a concurrent map leveraging a lock and standard map
// BEWARE: map.Range() must be followed by map.Unlock() to prevent
// a lock from remaining on the map.
type Map struct {
	sync.RWMutex
	m map[hashmaps.Key]hashmaps.Value
}

// New is a new concurrent map
func New(length int) *Map {
	return &Map{
		m: make(map[hashmaps.Key]hashmaps.Value, length),
	}
}

// Contains checks whether any entry is found
func (x *Map) Contains(i hashmaps.Key) bool {
	x.RLock()
	_, ok := x.m[i]
	x.RUnlock()
	return ok
}

// Get returns an entry (keeping it in the map)
func (x *Map) Get(i hashmaps.Key) hashmaps.Value {
	x.RLock()
	v := x.m[i]
	x.RUnlock()
	return v
}

// Len is the number of elements in the map
func (x *Map) Len() int {
	x.RLock()
	l := len(x.m)
	x.RUnlock()
	return l
}

// Range allows one to range over the map
// BEWARE: One must call map.Unlock() after the range is through
func (x *Map) Range() map[hashmaps.Key]hashmaps.Value {
	x.Lock()
	return x.m
}

// Set (over)writes an entry
func (x *Map) Set(i hashmaps.Key, v hashmaps.Value) {
	x.Lock()
	x.m[i] = v
	x.Unlock()
	return
}

// Delete removes an entry
func (x *Map) Delete(i hashmaps.Key) {
	x.Lock()
	delete(x.m, i)
	x.Unlock()
	return
}

// SequentialCopy produces a copy that is no longer under a lock
func (x *Map) SequentialCopy() map[interface{}]interface{} {
	c := make(map[interface{}]interface{}, x.Len())
	for i, v := range x.Range() {
		c[i] = v
	}
	x.Unlock()
	return c
}
