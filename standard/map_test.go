package standard

import "bitbucket.org/rhagenson/hashmaps"

var _ hashmaps.Map = new(Map)
var _ hashmaps.Ranger = new(Map)
