package hashmaps

// Key is the general state of how to lookup an Entry in the hashmap
type Key interface{}

// Value is the general state of a hashmap entry
type Value interface{}

// Map is the general class of operations on a hashmap
// Operations as seen in standard map implementation:
// 		Creation: make(map[Key]Value, length)
// 		Access: Value := map[Key]
// 		Clearing: map[Key] = nil
// 		Entering: map[Key] = Value
// 		Check: _, ok := map[Key]
type Map interface {
	Reader
	Writer
}

// Reader is the collection of methods that read from the internal map state
type Reader interface {
	Get(Key) Value // entry := Map.Get(Key)

	Contains(Key) bool // ok := Map.Contains(Key)

	Len() int // length := Map.Len()
}

// Writer is the collection of methods that write/change the internal map state
type Writer interface {
	Set(Key, Value) // Map.Set(Key, Value)

	Delete(Key) // Map.Delete(Key)
}

// Ranger is a hashmap that provides a standard sequential map for use in range
type Ranger interface {
	// Range can potential change the internal state and thus is a "writer" method
	Range() map[Key]Value // for key, value := range Map.Range() {...}
}
