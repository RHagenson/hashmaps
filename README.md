# hashmaps

A collection of hashmap implementations mostly to replace Go's default non-concurrent maps when concurrency is needed. By default, one should use the built-in map, but there are times when a concurrent map is necessary.

## Why would I want to wrap everything with `hashmaps.Key` and `hashmaps.Value`?

>  Don't.

Copy the code into another file and do a wonderful search-and-replace to change all `hashmaps.Key` to the value you need, same for `hashmaps.Value`. I would not use this library exactly as is, I would run a search-and-replace to properly type my key and values.

If I planned to search-and-replace, why not just use `interface{}` everywhere? Because one `interface{}` looks the same as every other `interface{}` so search-and-replace would not work. (See what I am going for?)

If copying is so terrible, then wrap one of the implementations in a struct of your own that simply does the type assertions internally so you are working with your types directly, but backed by my implementation. 
